#Install composer
composer install

#Change env.example
rename the file .env.example to .env

#Databse setup
Inside the .env file add the database name in the DB_DATABASE field

#Storage for images
Run php artisan storage:link command

#Create the databse
To create the database run the command php artisan migrate --seed


