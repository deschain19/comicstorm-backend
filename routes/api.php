<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ItemController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\FormatController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
/*********************************Items*****************************************/
Route::get('/items/all',[ItemController::class,'index']);
Route::get('/items/item-id/{item}',[ItemController::class,'itemid']);
Route::get('/items/{category}',[ItemController::class,'category']);
Route::post('/items/store',[ItemController::class,'store']);


/*********************************Genres****************************************/
Route::get('/genres/all',[GenreController::class,'index']);
/********************************Categories*************************************/
Route::get('/categories/all',[CategoryController::class,'index']);
/********************************Languages**************************************/
Route::get('/languages/all',[LanguageController::class,'index']);
Route::get('/language/{language}',[LanguageController::class,'language']);
/*********************************Formats***************************************/
Route::get('/formats/all',[FormatController::class,'index']);
Route::get('/format/{format}',[FormatController::class,'format']);
/***********************************Books***************************************/
Route::get('/books/{book}',[BookController::class,'index']);
/**********************************Cart*****************************************/
Route::post('/cart/store',[CartController::class,'store']);
Route::get('/cart',[CartController::class,'index']);
Route::delete('/cart/destroy',[CartController::class,'destroy']);