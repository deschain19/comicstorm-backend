<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ItemUsersController extends Controller
{
    public function store(User $user,Request $request){
        $user->items()->attach([$request->item]);
    }
}
