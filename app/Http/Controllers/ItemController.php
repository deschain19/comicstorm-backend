<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\Item;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
class ItemController extends Controller
{
    public function category($category){
        $items = Item::where('category_id', '=', $category)->paginate(16);
        return response()->json(['items'=>$items]);
    }
    public function itemid(Item $item){
        return response()->json(['item'=>$item]);
    }
    public function index(){
        $items = Item::all();
        return response()->json(['items'=>$items]);
    }

    public function store(Request $request){
        $request->validate([
            'title'=>'required|max:50',
            'ISBN'=>'required|max:17',
            'price'=>'required|max:5',
            'stock'=>'required|max:5',
            'synopsis'=>'required|max:800',
            'edition'=>'required|max:50',
            'pages'=>'required|max:5',
            'year'=>'required',
            //'img_url'=>'required',
            'author'=>'required',
            'language'=>'required',
            'format'=>'required',
            'genre_id'=>'required',
            'category_id'=>'required',
            
        ],
        
        [
            'title'=>'Este campo es obligatorio',
            'ISBN'=>'Este campo es obligatorio',
            'price'=>'Este campo es obligatorio',
            'stock'=>'Este campo es obligatorio',
            'synopsis'=>'Este campo es obligatorio',
            'edition'=>'Este campo es obligatorio',
            'pages'=>'Este campo es obligatorio',
            'year'=>'Este campo es obligatorio',
            //'img_url'=>'Este campo es obligatorio',
            'author'=>'Este campo es obligatorio',
            'language'=>'Este campo es obligatorio',
            'format'=>'Este campo es obligatorio',
            'genre_id'=>'Este campo es obligatorio',
            'category_id'=>'Este campo es obligatorio',
            
        ]);

        $url = '';
        
        if($request->hasFile('img_url')){
            //$file = $request->file('img_url')->store('public/images');

            $file = $request->file('img_url')->storeAs(
                'public/images', $request->file('img_url')->getClientOriginalName()
            );

            $url = 'http://comicstorm-backend.test'.Storage::url($file);

        }

        

        Item::create([
            'title'=>$request->title,
            'ISBN'=>$request->ISBN,
            'code'=>Str::random(18),
            'price'=>$request->price,
            'stock'=>$request->stock,
            'synopsis'=>$request->synopsis,
            'edition'=>$request->edition,
            'pages'=>$request->pages,
            'year'=>$request->year,
            'img_url'=>$url,//$item['img_url'],
            'author'=>$request->author,
            'language'=>$request->language,
            'format'=>$request->format,
            'genre_id'=>$request->genre_id,
            'category_id'=>$request->category_id,
            
        ]);
        

        return $url;
    }
 
}