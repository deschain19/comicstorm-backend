<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Item;
class CartController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'item_id'=>'required'
        ],
        [
            'item_id'=>'Este campo es obligatorio'
        ]);

        Cart::create([
            'item_id'=>$request->item_id
        ]);

        return response()->json(['Articulo'=>'Agregado con exito']);
    }

    public function index(){
        $cart = Cart::all();
        $items = [];
        foreach ($cart as $key => $item) {
            $foundItem = Item::findOrFail($item->item_id);
            array_push($items,$foundItem);
        }

        return response()->json(['items'=>$items,'cart'=>$cart]);
    }


    public function destroy(Request $request){
        Cart::destroy($request->id);
        return response()->json(['item'=>'Borrado con exito']);
    }   
}
