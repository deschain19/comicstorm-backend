<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    
    /*public function carts(){
        return $this->belongsToMany('App\Models\Cart')->withPivot('price');
        
    }*/

    public function cart_purchase()
    {
        return $this->hasMany(CartPurchase::class);
    }
}
