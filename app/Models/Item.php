<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'ISBN',
        'code',
        'price',
        'stock',
        'synopsis',
        'edition',
        'pages',
        'year',
        'img_url',
        'author',
        'language',
        'format',
        'genre_id',
        'category_id',
        
    ];

    

}
