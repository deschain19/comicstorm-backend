<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\PaymentMethod;
class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $paymentmethod=PaymentMethod::create([
                'number'=>'12',
                'name' => 'Julio',
                'exp_date'=>now(),
                'active_method' => 'Active',
                'user_id' => 1, // password
            ]);

    }
}
