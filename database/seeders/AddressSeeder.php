<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Address;
class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $address=Address::create([
            'full_name'=>'Julio Cesar',
            'street'=>'lamatanza',
            'postal_code'=>'90456',
            'phone_number'=>'6623897645',
            'active_address'=>'Active',
            'user_id'=>1

        ]);

    }
}
