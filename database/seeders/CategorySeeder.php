<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Manga',
        ]);
        Category::create([
            'name' => 'Book',
        ]);
        Category::create([
            'name' => 'Audio Book',
        ]);
        Category::create([
            'name' => 'Kindle',
        ]);
        Category::create([
            'name' => 'Comic',
        ]);
    }
}
