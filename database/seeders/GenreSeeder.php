<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Genre;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::create([
            'name' => 'Science Ficiton',
        ]);
        Genre::create([
            'name' => 'Action',
        ]);
        Genre::create([
            'name' => 'Adventure',
        ]);
   
        Genre::create([
            'name' => 'Thriller',
        ]);
        Genre::create([
            'name' => 'Romance',
        ]);
      
        Genre::create([
            'name' => 'Politics',
        ]);
        Genre::create([
            'name' => 'Religion',
        ]);

    }
}
