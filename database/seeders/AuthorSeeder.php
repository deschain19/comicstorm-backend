<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Author;
class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Author::create([
            'name' => 'Stephen King',
        ]);
        Author::create([
            'name' => 'Eric Nylund',
        ]);
        Author::create([
            'name' => 'Octavio Paz',
        ]);
        Author::create([
            'name' => 'Carlos Cuahutemoc Sanchez',
        ]);
        Author::create([
            'name' => 'Karen Travis',
        ]);
        Author::create([
            'name' => 'J. K. Rowling',
        ]);
        Author::create([
            'name' => 'Ray Bradbury',
        ]);
    }
}
