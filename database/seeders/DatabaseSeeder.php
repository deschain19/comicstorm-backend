<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

       //prueba 2   
        $this->call(GenreSeeder::class);
        $this->call(CategorySeeder::class);
        \App\Models\Item::factory(1000)->create();
        $this->call(UserSeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(AddressSeeder::class);

        $this->call(PurchaseSeeder::class);
        
        

    }
}
