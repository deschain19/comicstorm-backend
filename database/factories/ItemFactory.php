<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'ISBN' => Str::random(17),
            'code' => Str::random(18),
            'price'=>random_int(100,2400),
            'stock'=>random_int(1,150),
            'synopsis' => Str::random(800),
            'edition' => Str::random(10,15),
            'pages' => random_int(90,1125),
            'year' => $this->faker->dateTimeBetween('-1 years'),
            'img_url' => 'http://comicstorm-backend.test/storage/images/not-available.png',
            'author'=>$this->faker->name(),
            'language'=>'Spanish',
            'format'=>'Hardcover',
            'genre_id'=>random_int(1,6),
            'category_id'=>random_int(1,5),
            

        
        ];
    }
}
