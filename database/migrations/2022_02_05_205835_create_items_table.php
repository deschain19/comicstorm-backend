<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('title')->length(50)->unique();
            $table->string('ISBN')->length(17);
            $table->string('code')->length(18);
            $table->double('price')->length(5);
            $table->integer('stock')->length(5);
            $table->string('synopsis')->length(800);
            $table->string('edition')->length(50);
            $table->integer('pages')->length(5);
            $table->date('year');
            $table->string('img_url')->length(100);
            $table->string('author')->length(50);
            $table->string('language')->length(20);
            $table->string('format')->length(20);
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genres');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
